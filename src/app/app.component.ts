import { Component, OnInit } from '@angular/core';
import { AuthTokenService } from './services/auth-token.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'my-weather';
  isLoggedIn = false;
  username: string;

  constructor(private authToken: AuthTokenService) {}
  ngOnInit(): void {
    this.isLoggedIn = !!this.authToken.getToken();
    if (this.isLoggedIn) {
      const user = this.authToken.getUser();
      this.username = user.username;
    }
  }
  logout(): void {
    this.authToken.signOut();
    window.location.reload();
  }
}
