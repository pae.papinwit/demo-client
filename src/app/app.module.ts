import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgApexchartsModule } from 'ng-apexcharts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChatService } from './services/chat.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarMenuComponent } from './shared/sidebar-menu/sidebar-menu.component';
import { HeaderComponent } from './shared/header/header.component';
import { MapComponent } from './pages/map/map.component';
import { HomeComponent } from './pages/home/home.component';
import { ChartsComponent } from './pages/charts/charts.component';
import { TableComponent } from './pages/table/table.component';

import { HttpClientModule } from '@angular/common/http';
import { WeatherComponent } from './components/weather/weather.component';
import { Covid19Component } from './pages/covid19/covid19.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ChatComponent } from './pages/chat/chat.component';

import { ConfirmedValidator } from './_mods/confirmed.validator';
import { AuthGuard } from './_guard/auth.gaurd';
import { ChatboxComponent } from './components/chatbox/chatbox.component';
import { ChatboxListComponent } from './components/chatbox-list/chatbox-list.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarMenuComponent,
    ConfirmedValidator,
    HeaderComponent,
    MapComponent,
    HomeComponent,
    TableComponent,
    WeatherComponent,
    Covid19Component,
    ChartsComponent,
    LoginComponent,
    RegisterComponent,
    ChatComponent,
    ChatboxComponent,
    ChatboxListComponent
  ],
  imports: [FormsModule, ReactiveFormsModule, BrowserModule, AppRoutingModule, HttpClientModule, NgApexchartsModule],
  providers: [AuthGuard, ChatService],
  bootstrap: [AppComponent]
})
export class AppModule {}
