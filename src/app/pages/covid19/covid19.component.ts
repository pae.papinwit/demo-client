import { Component, OnInit, ViewChild } from '@angular/core';
import { CovidService } from '../../services/covid.service';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexFill,
  ApexYAxis,
  ApexTooltip,
  ApexTitleSubtitle,
  ApexPlotOptions,
  ApexXAxis
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis | ApexYAxis[];
  title: ApexTitleSubtitle;
  labels: string[];
  stroke: any; // ApexStroke;
  dataLabels: any; // ApexDataLabels;
  fill: ApexFill;
  tooltip: ApexTooltip;
  plotOptions: ApexPlotOptions;
};

@Component({
  selector: 'app-covid19',
  templateUrl: './covid19.component.html',
  styleUrls: ['./covid19.component.scss']
})
export class Covid19Component implements OnInit {
  @ViewChild('chart') chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  public chartOptions2: Partial<ChartOptions>;

  CovidData: any = [];
  CovidHistData: any = [];
  HistDeath: any = [];
  HistRecover: any = [];
  HistCase: any = [];
  DateLabel: any = [];

  ProvinceData: any = [];
  DeathProvice: any = [];
  ProvinceLabel: any = [];

  constructor(private covidService: CovidService) {}

  ngOnInit(): void {
    this.covidService.getData().subscribe((response: any) => {
      this.CovidData = response;
    });
    this.covidService.getHistData().subscribe((response) => {
      this.CovidHistData = response;
      for (let i = 0; i < this.CovidHistData.length; i++) {
        this.HistDeath[i] = this.CovidHistData[i].total_death;
        this.HistRecover[i] = this.CovidHistData[i].total_recovered;
        this.HistCase[i] = this.CovidHistData[i].total_case;
        this.DateLabel[i] = this.CovidHistData[i].txn_date;
      }

      //apex chart
      this.chartOptions = {
        series: [
          {
            type: 'bar',
            name: 'Recovered',
            data: this.HistRecover
          },
          {
            type: 'bar',
            name: 'Death',
            data: this.HistDeath
          },
          {
            name: 'Cases',
            data: this.HistCase
          }
        ],
        chart: {
          height: 500,
          type: 'line',
          stacked: true,
          background: '#F2F2F2'
        },
        title: {
          text: 'Covid Informations'
        },
        xaxis: {
          categories: this.DateLabel
        },
        plotOptions: {
          pie: {
            donut: {
              labels: {
                show: true,
                total: {
                  showAlways: true,
                  show: true
                }
              }
            }
          }
        }
      };
      this.covidService.getDataPerProvinces().subscribe((response: any) => {
        this.ProvinceData = response;
        for (let i = 0; i < this.ProvinceData.length; i++) {
          this.DeathProvice[i] = this.ProvinceData[i].total_death;
          this.ProvinceLabel[i] = this.ProvinceData[i].province;
        }

        this.chartOptions2 = {
          series: this.DeathProvice,
          chart: {
            height: 500,
            width: 500,
            type: 'donut',
            background: '#F2F2F2'
          },
          labels: this.ProvinceLabel,
          title: {
            text: 'Death from'
          }
        };
      });
    });
  }
}
