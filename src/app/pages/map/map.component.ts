import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../../services/weather.service';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  //Weather API
  weatherAP: any = [];

  //Mapbox
  map: any;
  mapbox = {
    accessToken: 'pk.eyJ1IjoiaXBhZW1hcGJveCIsImEiOiJjbDA0Z2V1cnMwdDJ1M2JxaTBlNTY3YTA3In0.w1UioYu86ybVNnKdY-XZRQ',
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    zoom: 12
  };
  marker: any;

  constructor(private weatherService: WeatherService) {}
  ngOnInit(): void {
    //Call GetWeather to obtain Lat/Long
    this.weatherService.getWeather().subscribe((response: any) => {
      this.weatherAP = response;
      console.log(this.weatherAP);

      //MAPBOX OPERRATE INSIDE WEATHER
      this.map = new mapboxgl.Map({
        accessToken: this.mapbox.accessToken,
        container: this.mapbox.container,
        style: this.mapbox.style,
        zoom: this.mapbox.zoom,
        center: [this.weatherAP.location.lon, this.weatherAP.location.lat]
      });
      //Add Marker to Mapbox
      this.marker = new mapboxgl.Marker({ color: '#ff0000' })
        .setLngLat([this.weatherAP.location.lon, this.weatherAP.location.lat])
        .addTo(this.map);
    });
  }
}
