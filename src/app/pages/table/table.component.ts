import { Component, OnInit } from '@angular/core';
import { CovidService } from '../../services/covid.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  covidData: any = [];
  province: any = [];
  provinceCase: any = [];
  provinceCaseNew: any = [];
  provinceDeath: any = [];
  provinceDeathNew: any = [];

  constructor(private covidService: CovidService) {}

  ngOnInit(): void {
    this.covidService.getDataPerProvinces().subscribe((response: any) => {
      this.covidData = response;
      for (let i = 0; i < this.covidData.length; i++) {
        this.province[i] = this.covidData[i].province;
        this.provinceCase[i] = this.covidData[i].total_case;
        this.provinceDeath[i] = this.covidData[i].total_death;
        this.provinceCaseNew[i] = this.covidData[i].new_case;
        this.provinceDeathNew[i] = this.covidData[i].new_death;
      }
    });
  }
}
