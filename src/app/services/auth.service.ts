import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthTokenService } from './auth-token.service';

const AUTH_API = 'http://localhost:5000/api/auth/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private authToken: AuthTokenService) {}

  login(cred): Observable<any> {
    return this.http.post(
      AUTH_API + 'login',
      {
        email: cred.email,
        password: cred.password
      },
      httpOptions
    );
  }
  register(user): Observable<any> {
    return this.http.post(
      AUTH_API + 'register',
      {
        name: user.name,
        email: user.email,
        password: user.password
      },
      httpOptions
    );
  }

  public isAuthenticated(): boolean {
    if (this.authToken.getToken()) {
      return true;
    } else {
      return false;
    }
  }
}
