import { Injectable } from '@angular/core';
import { delay, Observable } from 'rxjs';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { AuthTokenService } from './auth-token.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  socket = io.connect(environment.SOCKET_ENDPOINT);
  GetUser;
  userdata: any;
  AllUsers: any;
  onlineUser: any;
  constructor(private authToken: AuthTokenService) {}

  setupSocketConnection() {
    //this.socket = io.connect(environment.SOCKET_ENDPOINT);
    console.log('Connected to Socket Server');
    this.socket.on('Hello', (msg) => {
      console.log(msg);
    });
    /*  this.socket.on('connect', () => {
      //console.log(this.socket.id);
      
    });*/
    this.userdata = { id: this.authToken.getUser().id, email: this.authToken.getUser().email, sid: this.socket.id };
    //console.log(this.userdata);
    this.socket.emit('joined-user', this.userdata);

    this.socket.on('send-online-users', (data) => {
      //console.log(data);
      this.onlineUser = data;
      //console.log(this.onlineUser);
    });
  }

  joinRoom = (roomName) => {
    this.socket.emit('join', roomName);
  };

  sendMessage(data) {
    this.socket.emit('send_msg', data);
  }

  getOnlineUser() {
    return this.onlineUser;
  }

  testReceived() {
    let observable = new Observable<string>((observer) => {
      this.socket.on('send_msg', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  /*
  testsubscribeToMessages = async () => {
    return new Promise((reslove, reject) => {
      let themessage;
      this.socket.on('send_msg', (msg) => {
        //console.log('Room event received!: ' + msg);
        themessage = msg;
        console.log('Room themessage: ' + themessage);
        return reslove(themessage);
      });
      console.log('Room themessage after: ' + themessage);
    });
  };*/

  disconnect() {
    if (this.socket) {
      this.socket.emit('user-disconnected', this.authToken.getUser().email);
      this.socket.disconnect();
    }
  }
}
