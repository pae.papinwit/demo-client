import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  constructor(private http: HttpClient) {}

  getWeather() {
    return this.http.get(
      'http://api.weatherapi.com/v1/current.json?key=19223d28fd944df2ad7112939222702&q=Bangkok&aqi=no'
    );
  }
}
