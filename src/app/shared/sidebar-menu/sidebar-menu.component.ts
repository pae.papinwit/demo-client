import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { AuthTokenService } from '../../services/auth-token.service';

@Component({
  selector: 'app-sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
  styleUrls: ['./sidebar-menu.component.scss']
})
export class SidebarMenuComponent implements OnInit {
  constructor(private authToken: AuthTokenService, private chatService: ChatService) {}

  ngOnInit(): void {}
  logout(): void {
    this.chatService.disconnect();
    this.authToken.signOut();
    window.location.reload();
  }
}
